import json
import urllib.parse
import requests


class CustomResponse(requests.Response):
    def __init__(self, base_response, inah_response, session_cookies):
        super().__init__()
        for a in super().__attrs__:
            setattr(self, a, getattr(base_response, a))
        self._content = inah_response
        self.cookies = session_cookies

    @property
    def text(self):
        return self._content


def parse(data):
    parsed = urllib.parse.parse_qs(data)
    fixed = {}
    for k, v in parsed.items():
        if len(v) == 1:
            fixed[k] = v[0]
        else:
            fixed[k] = v
    return json.dumps(fixed)


class Session:
    def __init__(self, server_url, *args, custom_session=requests.Session, **kwargs):
        self._session = custom_session(*args, **kwargs)
        self._triggers = []
        self._server_url = server_url
        for f in ["delete", "get", "head", "options", "patch", "post", "put"]:
            setattr(
                self,
                f,
                self._generate_method(f),
            )

    def _generate_method(self, method):
        def f(*args, **kwargs):
            response = getattr(self._session, method)(*args, **kwargs)
            if self._is_inah_needed(response):
                inah_res = self._process_request(response)
                for _, cookie in inah_res.json()['cookies'].items():
                    del cookie['httpOnly']
                    keys = [k for k in cookie.keys()]
                    for k in keys:
                        if cookie[k] is None:
                            del cookie[k]
                    self._session.cookies.set(**cookie)
                response = CustomResponse(response, inah_res.json()['response'], self._session.cookies)
            return response

        return f

    def _is_inah_needed(self, response):
        for trigger in self._triggers:
            if trigger(response):
                return True
        return False

    def _process_request(self, response):
        data = {
            "url": response.request.url,
            "method": "GET" if response.request.method.lower() in ['get', 'head', 'options'] else "POST"
        }
        if data["method"] == "POST":
            data["body"] = parse(response.request.body)
        else:
            data["headers"] = response.request.headers
        res = self._session.post("{}/get-me-a-hooman".format(self._server_url), data=data)
        return res

    def add_trigger(self, trigger):
        self._triggers.append(trigger)

    def close(self, *args, **kwargs):
        self._session.close(*args, **kwargs)
