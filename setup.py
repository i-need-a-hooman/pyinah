import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyinah",
    version="1.0",
    author="Corentin CAM",
    author_email="cam.corentin@gmail.com",
    description="Python I Need A Hooman client",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/i-need-a-hooman/pyinah",
    packages=setuptools.find_packages(),
    project_urls={"Source Code": "https://gitlab.com/i-need-a-hooman/pyinah"},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["requests"],
)
